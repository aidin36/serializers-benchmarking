# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import time

import avro.schema
from avro.io import DatumWriter, DatumReader
from avro.datafile import DataFileWriter, DataFileReader

# For compatibility between both 2 and 3 versions
if sys.version_info[0] == 2:
    from StringIO import StringIO
    schema_parse = avro.schema.parse
else:
    from io import StringIO
    schema_parse = avro.schema.Parse


import utils
from test_object import deposit_dto


deposit_id_avro = \
'''{"namespace": "example.avro",
 "type": "record",
 "name": "DepositID",
 "fields": [
     {"name": "branch_code", "type": "int"},
     {"name": "type_code",  "type": "int"},
     {"name": "number", "type": "int"},
     {"name": "serial", "type": "int"}
 ]
}'''

currency_avro = \
'''{"namespace": "example.avro",
 "type": "record",
 "name": "Currency",
 "fields": [
     {"name": "id", "type": "string"}
  ]
}'''

deposit_related_customer_avro = \
'''{"namespace": "example.avro",
 "type": "record",
 "name": "DepositRelatedCustomer",
 "fields": [
     {"name": "id", "type": "int"},
     {"name": "name", "type": "string"},
     {"name": "book_id", "type": "int"},
     {"name": "father_name", "type": "string"},
     {"name": "job_id", "type": "int"},
     {"name": "relation_code", "type": "int"}
  ]
}'''

deposit_related_deposit_avro = \
'''{"namespace": "example.avro",
 "type": "record",
 "name": "DepositRelatedDeposit",
 "fields": [
     {"name": "id", "type": "DepositID"},
     {"name": "title", "type": "string"},
     {"name": "currency_id", "type": "string"},
     {"name": "state", "type": "int"}
  ]
}'''

deposit_dto_avro = \
'''
{"namespace": "bench",
 "type": "record",
 "name": "DepositDTO",
 "fields": [
     {"name": "id", "type":
        {"type": "record", "name": "DepositID", "fields": [
            {"name": "branch_code", "type": "int"},
            {"name": "type_code",  "type": "int"},
            {"name": "number", "type": "int"},
            {"name": "serial", "type": "int"}
        ]}
    },
    {"name": "currency_id", "type": "string"},
    {"name": "currency", "type":
        {"type": "record", "name": "Currency", "fields": [
            {"name": "id", "type": "string"},
            {"name": "description", "type": "string"},
            {"name": "latin_description", "type": "string"}
        ]}
    },
    {"name": "title", "type": "string"},
    {"name": "has_card", "type": "int"},
    {"name": "has_prize", "type": "int"},
    {"name": "state", "type": "int"},
    {"name": "balance", "type": "double"},
    {"name": "blocked_amount", "type": "double"},
    {"name": "related_customers", "type":
        {"type": "array", "items":
            {"name": "RelatedCustomer", "type": "record", "fields": [
                {"name": "id", "type": "int"},
                {"name": "name", "type": "string"},
                {"name": "book_id", "type": "int"},
                {"name": "father_name", "type": "string"},
                {"name": "job_id", "type": "int"},
                {"name": "relation_code", "type": "int"}
            ]}
        }
    },
    {"name": "related_deposits", "type":
        {"type": "array", "items":
            {"name": "RelatedDeposit", "type": "record", "fields": [
                {"name": "id", "type": "DepositID"},
                {"name": "title", "type": "string"},
                {"name": "currency_id", "type": "string"},
                {"name": "state", "type": "int"}
            ]}
        }
    }
 ]
}
'''

def test_avro(no_of_loops):
    schema = schema_parse(deposit_dto_avro)

    # Warm up
    for i in xrange(30):
        output_io = StringIO()
        writer = DataFileWriter(output_io, DatumWriter(), schema)
        writer.append(deposit_dto)
        writer.flush()
        avro_result = output_io.getvalue()
        writer.close()

    start_time = time.time()
    for i in xrange(no_of_loops):
        output_io = StringIO()
        writer = DataFileWriter(output_io, DatumWriter(), schema)
        writer.append(deposit_dto)
        writer.flush()
        avro_result = output_io.getvalue()
        writer.close()
    pack_time = time.time() - start_time

    # Warm up
    for i in xrange(30):
        input_io = StringIO()
        input_io.write(avro_result)
        reader = DataFileReader(input_io, DatumReader())

    start_time = time.time()
    for i in xrange(no_of_loops):
        input_io = StringIO()
        input_io.write(avro_result)
        reader = DataFileReader(input_io, DatumReader())
        # Forcing the reader to parse the data.
        reader.next()

    unpack_time = time.time() - start_time

    return (len(avro_result), pack_time, unpack_time)

no_of_loops = int(sys.argv[1])
test_result = test_avro(no_of_loops)
print("Apache Avro,{0[0]},{0[1]},{0[2]}".format(test_result))

