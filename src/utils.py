# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys

# For compatibility between both 2 and 3 versions
if sys.version_info[0] == 3:
    __builtins__['xrange'] = range

