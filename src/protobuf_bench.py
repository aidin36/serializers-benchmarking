#coding: utf-8
# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import time

import utils
import depositdto_pb2


def create_test_object():
    deposit_dto = depositdto_pb2.DepositDTO()

    deposit_id = depositdto_pb2.DepositID()
    deposit_id.branch_code = 1001
    deposit_id.type_code = 800
    deposit_id.number = 18499
    deposit_id.serial = 1

    currency = depositdto_pb2.Currency()
    currency.id = "IRR"
    currency.description = u'ریال'
    currency.lating_description = 'Rial'

    related_customer_1 = deposit_dto.related_customers.add()
    related_customer_1.id = 187733
    related_customer_1.name = u'ﻊﺑﺎﺳ ﻖﻟی ﺥﺎﻧ'
    related_customer_1.book_id = 198833
    related_customer_1.father_name = 'ﻊﻟی ﻡﺭﺩﺎﻧ'
    related_customer_1.job_id = 5
    related_customer_1.relation_code = 0

    related_customer_2 = deposit_dto.related_customers.add()
    related_customer_2.id = 183233
    related_customer_2.name = u'ﻢﺤﻣﺩ ﺮﺿﺍ'
    related_customer_2.book_id = 1933
    related_customer_2.father_name = u'ﺎﺤﻣﺩ'
    related_customer_2.job_id = 2
    related_customer_2.relation_code = 2

    related_deposit = deposit_dto.related_deposits.add()
    related_deposit_id = depositdto_pb2.DepositID()
    related_deposit_id.branch_code = 1001
    related_deposit_id.type_code = 820
    related_deposit_id.number = 18991
    related_deposit_id.serial = 7
    related_deposit.id.CopyFrom(related_deposit_id)
    related_deposit.title = u'ﺱپﺭﺪﻫ ﻭﺍﺭیﺯ ﺱﻭﺩ ﺶﻣﺍﺮﻫ یک'
    related_deposit.currency_id = 'IRR'
    related_deposit.state = 0

    deposit_dto.deposit_id.CopyFrom(deposit_id)
    deposit_dto.currency_id = 'IRR'
    deposit_dto.currency.CopyFrom(currency)
    deposit_dto.title = u'ﺱپﺭﺪﻫ ﺱﺮﻣﺍیﻩ گﺫﺍﺭی کﻮﺗﺎﻫ ﻡﺪﺗ ﺂﻗﺍی ﻑﻼﻧی'
    deposit_dto.has_card = True
    deposit_dto.has_prize = False
    deposit_dto.state = 0
    deposit_dto.balance = 12200000
    deposit_dto.blocked_amount = 190000

    return deposit_dto


def test_protobuf(no_of_loops):
    # Warmup
    for i in xrange(30):
        deposit_dto = create_test_object()
        protobuf_result = deposit_dto.SerializeToString()

    start_time = time.time()
    for i in xrange(no_of_loops):
        deposit_dto = create_test_object()
        protobuf_result = deposit_dto.SerializeToString()

    pack_time = time.time() - start_time

    # Warmup
    for i in xrange(30):
        deposit_dto = depositdto_pb2.DepositDTO()
        deposit_dto.ParseFromString(protobuf_result)

    start_time = time.time()
    for i in xrange(no_of_loops):
        deposit_dto = depositdto_pb2.DepositDTO()
        deposit_dto.ParseFromString(protobuf_result)
    unpack_time = time.time() - start_time

    return (len(protobuf_result), pack_time, unpack_time)

no_of_loops = int(sys.argv[1])
test_result = test_protobuf(no_of_loops)
print("Protocol Buffers,{0[0]},{0[1]},{0[2]}".format(test_result))


