#coding: utf-8
# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import time

import utils

from soaplib.serializers.clazz import ClassSerializer, Array
from soaplib.serializers.primitive import *

from lxml import etree


class DepositID(ClassSerializer):
    branch_code = Integer
    type_code = Integer
    number = Integer
    serial = Integer

class Currency(ClassSerializer):
    id = String
    description = String
    latin_description = String

class DepositRelatedCustomer(ClassSerializer):
    id = Integer
    name = String
    father_name = String
    book_id = Integer
    job_id = Integer
    relation_code = Integer

class DepositRelatedDeposit(ClassSerializer):
    id = DepositID
    title = String
    currency_id = String
    state = Integer

class DepositDTO(ClassSerializer):
    id = DepositID
    currency_id = String
    currency = Currency
    title = String
    had_card = Boolean
    has_prize = Boolean
    state = Integer
    balance = Double
    blocked_amount = Double
    related_customers = Array(DepositRelatedCustomer)
    related_deposits = Array(DepositRelatedDeposit)


def create_deposit_object():
    deposit_id = DepositID(branch_code=1001, type_code=800, number=48225, serial=1)
    customer_1 = DepositRelatedCustomer(**{'id': 187733, 'name': 'ﻊﺑﺎﺳ ﻖﻟی ﺥﺎﻧ', 'book_id': 198833, 'father_name': 'ﻊﻟی ﻡﺭﺩﺎﻧ', 'job_id': 5, 'relation_code': 0})
    customer_2 = DepositRelatedCustomer(**{'id': 183233, 'name': 'ﻢﺤﻣﺩ ﺮﺿﺍ', 'book_id': 1933, 'father_name': 'ﺎﺤﻣﺩ', 'job_id': 2, 'relation_code': 2})
    currency = Currency(**{'id': 'IRR', 'description': 'ﺭیﺎﻟ', 'latin_description': 'Rial'})
    deposit_id_2 = DepositID(**{'branch_code': 1001, 'type_code': 820, 'number': 18991, 'serial': 7})
    related_deposit = DepositRelatedDeposit(**{'id': deposit_id_2, 'title': 'ﺱپﺭﺪﻫ ﻭﺍﺭیﺯ ﺱﻭﺩ ﺶﻣﺍﺮﻫ یک', 'currency_id': 'IRR', 'state': 0})

    deposit = DepositDTO(id=deposit_id, currency_id='IRR', currency=currency, title='ﺱپﺭﺪﻫ ﺱﺮﻣﺍیﻩ گﺫﺍﺭی کﻮﺗﺎﻫ ﻡﺪﺗ ﺂﻗﺍی ﻑﻼﻧی', has_card=True, has_prize=True,
                         state=0, balance=12200000, blocked_amount=190000, related_customers=[customer_1, customer_2], related_deposits=[related_deposit])

    return deposit

def test_soap(no_of_loops):
    deposit = create_deposit_object()

    # Warmup
    for i in xrange(30):
        deposit_xml = etree.Element('deposit')
        DepositDTO.to_xml(deposit, DepositDTO.get_namespace(), deposit_xml)

        xml_result = etree.tostring(deposit_xml)

    # Packing test
    start_time = time.time()
    for i in xrange(no_of_loops):
        deposit_xml = etree.Element('deposit')
        DepositDTO.to_xml(deposit, DepositDTO.get_namespace(), deposit_xml)

        xml_result = etree.tostring(deposit_xml)
        
    pack_time = time.time() - start_time
    pack_size = len(xml_result)

    # Warmup unpacking
    for i in xrange(30):
        deposit_xml = etree.fromstring(xml_result)
        unpacked_deposit = DepositDTO.from_xml(deposit_xml[0])

    start_time = time.time()
    for i in xrange(no_of_loops):
        deposit_xml = etree.fromstring(xml_result)
        unpacked_deposit = DepositDTO.from_xml(deposit_xml[0])

    unpack_time = time.time() - start_time
    
    return (pack_size, pack_time, unpack_time)


no_of_loops = int(sys.argv[1])
test_result = test_soap(no_of_loops)
print("SOAP,{0[0]},{0[1]},{0[2]}".format(test_result))


