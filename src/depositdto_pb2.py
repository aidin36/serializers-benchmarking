# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: depositdto.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='depositdto.proto',
  package='compare_test',
  serialized_pb=_b('\n\x10\x64\x65positdto.proto\x12\x0c\x63ompare_test\"S\n\tDepositID\x12\x13\n\x0b\x62ranch_code\x18\x64 \x02(\x05\x12\x11\n\ttype_code\x18\x65 \x02(\x05\x12\x0e\n\x06number\x18\x66 \x02(\x05\x12\x0e\n\x06serial\x18g \x02(\x05\"G\n\x08\x43urrency\x12\n\n\x02id\x18h \x02(\t\x12\x13\n\x0b\x64\x65scription\x18i \x02(\t\x12\x1a\n\x12lating_description\x18j \x01(\t\"x\n\x0fRelatedCustomer\x12\n\n\x02id\x18k \x02(\x05\x12\x0c\n\x04name\x18l \x02(\t\x12\x0f\n\x07\x62ook_id\x18m \x02(\x05\x12\x13\n\x0b\x66\x61ther_name\x18n \x02(\t\x12\x0e\n\x06job_id\x18o \x02(\x05\x12\x15\n\rrelation_code\x18p \x02(\x05\"h\n\x0eRelatedDeposit\x12#\n\x02id\x18q \x02(\x0b\x32\x17.compare_test.DepositID\x12\r\n\x05title\x18r \x02(\t\x12\x13\n\x0b\x63urrency_id\x18s \x02(\t\x12\r\n\x05state\x18t \x01(\x05\"\xd6\x02\n\nDepositDTO\x12+\n\ndeposit_id\x18u \x02(\x0b\x32\x17.compare_test.DepositID\x12\x13\n\x0b\x63urrency_id\x18v \x02(\t\x12(\n\x08\x63urrency\x18w \x01(\x0b\x32\x16.compare_test.Currency\x12\r\n\x05title\x18x \x02(\t\x12\x10\n\x08has_card\x18y \x02(\x08\x12\x11\n\thas_prize\x18z \x02(\x08\x12\r\n\x05state\x18{ \x02(\x05\x12\x0f\n\x07\x62\x61lance\x18| \x01(\x01\x12\x16\n\x0e\x62locked_amount\x18} \x01(\x01\x12\x38\n\x11related_customers\x18~ \x03(\x0b\x32\x1d.compare_test.RelatedCustomer\x12\x36\n\x10related_deposits\x18\x7f \x03(\x0b\x32\x1c.compare_test.RelatedDeposit')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_DEPOSITID = _descriptor.Descriptor(
  name='DepositID',
  full_name='compare_test.DepositID',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='branch_code', full_name='compare_test.DepositID.branch_code', index=0,
      number=100, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='type_code', full_name='compare_test.DepositID.type_code', index=1,
      number=101, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='number', full_name='compare_test.DepositID.number', index=2,
      number=102, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='serial', full_name='compare_test.DepositID.serial', index=3,
      number=103, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=34,
  serialized_end=117,
)


_CURRENCY = _descriptor.Descriptor(
  name='Currency',
  full_name='compare_test.Currency',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='compare_test.Currency.id', index=0,
      number=104, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='description', full_name='compare_test.Currency.description', index=1,
      number=105, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='lating_description', full_name='compare_test.Currency.lating_description', index=2,
      number=106, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=119,
  serialized_end=190,
)


_RELATEDCUSTOMER = _descriptor.Descriptor(
  name='RelatedCustomer',
  full_name='compare_test.RelatedCustomer',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='compare_test.RelatedCustomer.id', index=0,
      number=107, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='name', full_name='compare_test.RelatedCustomer.name', index=1,
      number=108, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='book_id', full_name='compare_test.RelatedCustomer.book_id', index=2,
      number=109, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='father_name', full_name='compare_test.RelatedCustomer.father_name', index=3,
      number=110, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='job_id', full_name='compare_test.RelatedCustomer.job_id', index=4,
      number=111, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='relation_code', full_name='compare_test.RelatedCustomer.relation_code', index=5,
      number=112, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=192,
  serialized_end=312,
)


_RELATEDDEPOSIT = _descriptor.Descriptor(
  name='RelatedDeposit',
  full_name='compare_test.RelatedDeposit',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='compare_test.RelatedDeposit.id', index=0,
      number=113, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='title', full_name='compare_test.RelatedDeposit.title', index=1,
      number=114, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='currency_id', full_name='compare_test.RelatedDeposit.currency_id', index=2,
      number=115, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='state', full_name='compare_test.RelatedDeposit.state', index=3,
      number=116, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=314,
  serialized_end=418,
)


_DEPOSITDTO = _descriptor.Descriptor(
  name='DepositDTO',
  full_name='compare_test.DepositDTO',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='deposit_id', full_name='compare_test.DepositDTO.deposit_id', index=0,
      number=117, type=11, cpp_type=10, label=2,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='currency_id', full_name='compare_test.DepositDTO.currency_id', index=1,
      number=118, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='currency', full_name='compare_test.DepositDTO.currency', index=2,
      number=119, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='title', full_name='compare_test.DepositDTO.title', index=3,
      number=120, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='has_card', full_name='compare_test.DepositDTO.has_card', index=4,
      number=121, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='has_prize', full_name='compare_test.DepositDTO.has_prize', index=5,
      number=122, type=8, cpp_type=7, label=2,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='state', full_name='compare_test.DepositDTO.state', index=6,
      number=123, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='balance', full_name='compare_test.DepositDTO.balance', index=7,
      number=124, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='blocked_amount', full_name='compare_test.DepositDTO.blocked_amount', index=8,
      number=125, type=1, cpp_type=5, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='related_customers', full_name='compare_test.DepositDTO.related_customers', index=9,
      number=126, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='related_deposits', full_name='compare_test.DepositDTO.related_deposits', index=10,
      number=127, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=421,
  serialized_end=763,
)

_RELATEDDEPOSIT.fields_by_name['id'].message_type = _DEPOSITID
_DEPOSITDTO.fields_by_name['deposit_id'].message_type = _DEPOSITID
_DEPOSITDTO.fields_by_name['currency'].message_type = _CURRENCY
_DEPOSITDTO.fields_by_name['related_customers'].message_type = _RELATEDCUSTOMER
_DEPOSITDTO.fields_by_name['related_deposits'].message_type = _RELATEDDEPOSIT
DESCRIPTOR.message_types_by_name['DepositID'] = _DEPOSITID
DESCRIPTOR.message_types_by_name['Currency'] = _CURRENCY
DESCRIPTOR.message_types_by_name['RelatedCustomer'] = _RELATEDCUSTOMER
DESCRIPTOR.message_types_by_name['RelatedDeposit'] = _RELATEDDEPOSIT
DESCRIPTOR.message_types_by_name['DepositDTO'] = _DEPOSITDTO

DepositID = _reflection.GeneratedProtocolMessageType('DepositID', (_message.Message,), dict(
  DESCRIPTOR = _DEPOSITID,
  __module__ = 'depositdto_pb2'
  # @@protoc_insertion_point(class_scope:compare_test.DepositID)
  ))
_sym_db.RegisterMessage(DepositID)

Currency = _reflection.GeneratedProtocolMessageType('Currency', (_message.Message,), dict(
  DESCRIPTOR = _CURRENCY,
  __module__ = 'depositdto_pb2'
  # @@protoc_insertion_point(class_scope:compare_test.Currency)
  ))
_sym_db.RegisterMessage(Currency)

RelatedCustomer = _reflection.GeneratedProtocolMessageType('RelatedCustomer', (_message.Message,), dict(
  DESCRIPTOR = _RELATEDCUSTOMER,
  __module__ = 'depositdto_pb2'
  # @@protoc_insertion_point(class_scope:compare_test.RelatedCustomer)
  ))
_sym_db.RegisterMessage(RelatedCustomer)

RelatedDeposit = _reflection.GeneratedProtocolMessageType('RelatedDeposit', (_message.Message,), dict(
  DESCRIPTOR = _RELATEDDEPOSIT,
  __module__ = 'depositdto_pb2'
  # @@protoc_insertion_point(class_scope:compare_test.RelatedDeposit)
  ))
_sym_db.RegisterMessage(RelatedDeposit)

DepositDTO = _reflection.GeneratedProtocolMessageType('DepositDTO', (_message.Message,), dict(
  DESCRIPTOR = _DEPOSITDTO,
  __module__ = 'depositdto_pb2'
  # @@protoc_insertion_point(class_scope:compare_test.DepositDTO)
  ))
_sym_db.RegisterMessage(DepositDTO)


# @@protoc_insertion_point(module_scope)
