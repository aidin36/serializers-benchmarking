# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import time
import json

import utils
from test_object import deposit_dto


def test_json(no_of_loops):
    # Warm up
    for i in xrange(30):
        json_result = json.dumps(deposit_dto)

    # The test
    start_time = time.time()
    for i in xrange(no_of_loops):
        json_result = json.dumps(deposit_dto)
    pack_time = time.time() - start_time

    # Warm up
    for i in xrange(30):
        json.loads(json_result)

    start_time = time.time()
    for i in xrange(no_of_loops):
        json.loads(json_result)
    unpack_time = time.time() - start_time

    return (len(json_result), pack_time, unpack_time)

no_of_loops = int(sys.argv[1])
test_result = test_json(no_of_loops)
print("JSON,{0[0]},{0[1]},{0[2]}".format(test_result))

