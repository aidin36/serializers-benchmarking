#coding: utf-8
# This file is part of "serializers-benchmarking" project.
# <https://gitlab.com/aidin36/serializers-benchmarking>
# Copyright (C) 2017, Aidin Gharibnavaz <aidin@aidinhut.com>
#
# "serializers-benchmarking" is free software: you can redistribute
# it and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# "serializers-benchmarking" is distributed in the hope that it will
# be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file. If not, see <http://www.gnu.org/licenses/>.

import sys
import time
import bson

import utils

# BSON face an error with unicode strings.
# So it cannot use the original test object.

deposit_dto = {'id': {'branch_code': 1001, 'type_code': 800, 'number': 18499, 'serial': 1},
               'currency_id': 'IRR',
               'currency': {'id': 'IRR', 'description': u'ریال'.encode('utf-8'), 'latin_description': 'Rial'},
               'title': u'سپرده سرمایه گذاری کوتاه مدت آقای فلانی'.encode('utf-8'),
               'has_card': True,
               'has_prize': False,
               'state': 0,
               'balance': 12200000,
               'blocked_amount': 190000,
               'related_customers': [{'id': 187733, 'name': u'عباس قلی خان'.encode('utf-8'), 'book_id': 198833, 'father_name': u'علی مردان'.encode('utf-8'), 'job_id': 5, 'relation_code': 0},
                                     {'id': 183233, 'name': u'محمد رضا'.encode('utf-8'), 'book_id': 1933, 'father_name': u'احمد'.encode('utf-8'), 'job_id': 2, 'relation_code': 2}],
               'related_deposits': [{'id': {'branch_code': 1001, 'type_code': 820, 'number': 18991, 'serial': 7}, 'title': u'سپرده واریز سود شماره یک'.encode('utf-8'), 'currency_id': 'IRR', 'state': 0}],
               }



def test_bson(no_of_loops):
    # Warm up
    for i in xrange(30):
        bson_result = bson.dumps(deposit_dto)

    start_time = time.time()
    for i in xrange(no_of_loops):
        bson_result = bson.dumps(deposit_dto)
    pack_time = time.time() - start_time

    # Warm up
    for i in xrange(30):
        bson.loads(bson_result)

    start_time = time.time()
    for i in xrange(no_of_loops):
        bson.loads(bson_result)
    unpack_time = time.time() - start_time

    return (len(bson_result), pack_time, unpack_time)


no_of_loops = int(sys.argv[1])
test_result = test_bson(no_of_loops)
print("BSON,{0[0]},{0[1]},{0[2]}".format(test_result))
