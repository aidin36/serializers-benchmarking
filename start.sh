#!/bin/sh

LOOPS=50000

cd src/

python2 avro_bench.py $LOOPS
python2 bson_bench.py $LOOPS
python2 json_bench.py $LOOPS
python2 msgpack_bench.py $LOOPS
python2 protobuf_bench.py $LOOPS
python2 soap_bench.py $LOOPS

